FROM  centos:7.3.1611

RUN yum install -y wget which

RUN wget https://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo

RUN sed -i s/\$releasever/7/g /etc/yum.repos.d/epel-apache-maven.repo

RUN yum install -y apache-maven

RUN mvn --version

COPY m2.tar.xz /tmp/
RUN cd /tmp/ && tar -Jxvf m2.tar.xz && mv m2 /root/.m2 && rm -rf m2.tar.xz


#ADD ./project /project
WORKDIR /project

#Time
ENV TW=Asia/Taipei
RUN ln -snf /usr/share/zoneinfo/$TW /etc/localtime && echo $TW > /etc/timezone

#RUN mvn clean

#CMD ["/bin/bash"]


CMD mvn package
